from django.test import TestCase

from accounts.models import User

DEV_EMAIL = "dev.user@listpaper.io"
TEST_EMAIL = "test.user@listpaper.io"

DEV_PASSWORD = "test1234"
TEST_PASSWORD = "test1234"


########################################################################################################################
class BaseListpaperTest(TestCase):

    ####################################################################################################################
    @classmethod
    def setUpTestData(cls):
        super(BaseListpaperTest, cls).setUpTestData()
        User.objects.all().delete()

        cls.super_user = User.objects.create(email=DEV_EMAIL, password=DEV_PASSWORD, is_staff=True, is_active=True)
        cls.super_user.set_password(DEV_PASSWORD)
        cls.super_user.save()

        cls.user = User.objects.create(email=TEST_EMAIL, password=TEST_PASSWORD,
                                       is_active=True, timezone_slug="US/Central")
        cls.user.set_password(TEST_PASSWORD)
        cls.user.save()

    ####################################################################################################################
    @classmethod
    def tearDownClass(cls):
        User.objects.all().delete()

    ####################################################################################################################
    def login_staff(self):
        logged_in = self.client.login(username=self.super_user.email, password=DEV_PASSWORD)
        self.assertTrue(logged_in)

    ####################################################################################################################
    def login(self):
        logged_in = self.client.login(username=self.user.email, password=TEST_PASSWORD)
        self.assertTrue(logged_in)
