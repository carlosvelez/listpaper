from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import redirect


########################################################################################################################
class DotworkAuthMixin(object):

    ####################################################################################################################
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(DotworkAuthMixin, cls).as_view(**initkwargs)
        return login_required(view)

    ##################################################################################################################
    def dispatch(self, request, *args, **kwargs):
        if self.request.user and self.request.user.is_authenticated():
            return super(DotworkAuthMixin, self).dispatch(request, *args, **kwargs)
        else:
            return redirect(reverse(login))
