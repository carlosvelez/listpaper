from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from accounts.urls import urlpatterns as account_urlpatterns
from api.urls import urlpatterns as api_urlpatterns
from webapp.urls import urlpatterns as webapp_urlpatterns


########################################################################################################################
urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

urlpatterns += account_urlpatterns
urlpatterns += api_urlpatterns
urlpatterns += webapp_urlpatterns

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
