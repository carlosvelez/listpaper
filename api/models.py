from django.db import models
from accounts.models import User
from base_models import BaseListpaperModel


########################################################################################################################
class Listpaper(BaseListpaperModel):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=172, blank=True, null=True)

    ####################################################################################################################
    def __str__(self):
        return self.name or "Listpaper {timestamp}".format(timestamp=self.created)


########################################################################################################################
class ListpaperItem(BaseListpaperModel):
    text = models.CharField(verbose_name="Text", max_length=720)
    list = models.ForeignKey(Listpaper, related_name="items")

    ####################################################################################################################
    def __str__(self):
        return self.text or "<empty>"
