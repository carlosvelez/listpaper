from django.contrib import admin

from api import models as webapp_models


########################################################################################################################
@admin.register(webapp_models.Listpaper)
class ListpaperAdmin(admin.ModelAdmin):
    pass


########################################################################################################################
@admin.register(webapp_models.ListpaperItem)
class ListpaperItemAdmin(admin.ModelAdmin):
    pass
