from rest_framework import serializers

from api.models import ListpaperItem, Listpaper


########################################################################################################################
class ListpaperItemSerializer(serializers.HyperlinkedModelSerializer):
    list = serializers.HyperlinkedRelatedField(view_name="listpaper-detail", read_only=True)

    ####################################################################################################################
    def get_queryset(self):
        return ListpaperItem.objects.filter(list__user=self.request.user)

    ####################################################################################################################
    def create(self, validated_data):
        """

        :param validated_data:
        :return:
        """
        return ListpaperItem.objects.create(**validated_data)

    ####################################################################################################################
    def update(self, instance, validated_data):
        """

        :param instance:
        :param validated_data:
        :return:
        """
        instance.text = validated_data.get("text", instance.text)
        instance.list = validated_data.get("list", instance.list)
        instance.save()
        instance.refresh_from_db()
        return instance

    ####################################################################################################################
    class Meta:
        model = ListpaperItem
        fields = ("pk", "url", "text", "list", "created", "last_modified")


########################################################################################################################
class ListpaperSerializer(serializers.HyperlinkedModelSerializer):
    items = ListpaperItemSerializer(many=True, read_only=True)
    user = serializers.HyperlinkedRelatedField(view_name="user-detail", read_only=True)

    ####################################################################################################################
    def get_queryset(self):
        return Listpaper.objects.filter(user=self.request.user)

    ####################################################################################################################
    def create(self, validated_data):
        """

        :param validated_data:
        :return:
        """
        return Listpaper.objects.create(**validated_data)

    ####################################################################################################################
    def update(self, instance, validated_data):
        """

        :param instance:
        :param validated_data:
        :return:
        """
        instance.name = validated_data.get("name", instance.name)
        instance.save()
        instance.refresh_from_db()
        return instance

    ####################################################################################################################
    class Meta:
        model = Listpaper
        fields = ("pk", "url", "name", "items", "user", "created", "last_modified")
        ordering = ("last_modified", )
