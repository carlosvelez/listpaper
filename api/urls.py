from django.conf.urls import url, include

import api.views as api_views


########################################################################################################################
urlpatterns = [
    url(r'^api/$', api_views.api_root, name="root"),
    url(r'^api/listpapers/$', api_views.ListpaperAPIView.as_view(), name="listpapers"),
    url(r'^api/listpapers/(?P<pk>\d+)/$', api_views.ListpaperDetailAPIView.as_view(), name="listpaper-detail"),
    url(r'^api/listpaperitems/(?P<pk>\d+)/$', api_views.ListpaperItemDetailAPIView.as_view(), name="listpaperitem-detail"),
    url(r"^api-auth/", include("rest_framework.urls")),
]
