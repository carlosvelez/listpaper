import datetime
from datetimemocker.datetimemocker import DateTimeMocker
from collections import OrderedDict

import pytz
from django.core.urlresolvers import reverse

from api.models import Listpaper, ListpaperItem
from api.test.utils import BaseListpaperAPITest


########################################################################################################################
class ListpaperAPIViewTest(BaseListpaperAPITest):

    ####################################################################################################################
    @classmethod
    def setUpTestData(cls):
        super(ListpaperAPIViewTest, cls).setUpTestData()
        cls.url = reverse("listpapers")

    ####################################################################################################################
    def setUp(self):
        super(ListpaperAPIViewTest, self).setUp()
        self.expected_data = [
            OrderedDict([
                ('pk', self.listpaper.pk),
                ('url', 'http://testserver/api/listpapers/{}/'.format(self.listpaper.pk)),
                ('name', self.listpaper.name),
                ('items', [
                    OrderedDict([
                        ('pk', self.item_1.pk),
                        ('url', 'http://testserver/api/listpaperitems/{}/'.format(self.item_1.pk)),
                        ('text', 'Item 1'),
                        ('list', 'http://testserver/api/listpapers/{}/'.format(self.listpaper.pk)),
                        ('created', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_1.created)),
                        ('last_modified', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_1.last_modified)),
                    ]),
                    OrderedDict([
                        ('pk', self.item_2.pk),
                        ('url', 'http://testserver/api/listpaperitems/{}/'.format(self.item_2.pk)),
                        ('text', 'Item 2'),
                        ('list', 'http://testserver/api/listpapers/{}/'.format(self.listpaper.pk)),
                        ('created', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_2.created)),
                        ('last_modified', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_2.last_modified)),
                    ])
                ]),
                ('user', 'http://testserver/api/users/{}/'.format(self.user.pk)),
                ('created', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.listpaper.created)),
                ('last_modified', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.listpaper.last_modified)),
            ])
        ]

    ####################################################################################################################
    def test_get(self):  # GET
        """Should return list of Listpaper objects."""
        response = self.api_client.get(self.url)
        self.assert_status_ok(response, expected=self.expected_data)

    ####################################################################################################################
    def test_create(self):  # POST
        """Should create a new Listpaper with a valid POST."""
        # Only 1 Listpaper should exist
        self.assertEqual(1, Listpaper.objects.count())

        # Do POST. Should succeed.
        data = {"name": "thank you"}
        with DateTimeMocker():
            response = self.api_client.post(self.url, data=data)

        self.assertEqual(2, Listpaper.objects.count())
        pk = Listpaper.objects.last().pk
        expected_data = {'user': 'http://testserver/api/users/{}/'.format(self.user.id),
                         'items': [],
                         'pk': pk,
                         'url': 'http://testserver/api/listpapers/{}/'.format(pk),
                         'created': self.mock_datetime_str,
                         'last_modified': self.mock_datetime_str,
                         'name': 'thank you'}
        self.assert_created(response, expected=expected_data)

    ####################################################################################################################
    def test_edit(self):  # PUT
        """Do not support editing a listpaper on the list page."""
        put = self.api_client.put(self.url)
        self.assert_not_allowed(put)

    ####################################################################################################################
    def test_delete(self):  # DELETE
        """Do not support deleting a listpaper on the list page."""
        delete = self.api_client.delete(self.url)
        self.assert_not_allowed(delete)


########################################################################################################################
class ListpaperDetailAPIViewTest(BaseListpaperAPITest):

    ####################################################################################################################
    def setUp(self):
        super(ListpaperDetailAPIViewTest, self).setUp()
        self.url = reverse("listpaper-detail", args=[self.item_1.pk])
        self.expected_data = {
            'name': 'Test List',
            'url': 'http://testserver/api/listpapers/{}/'.format(self.listpaper.pk),
            'pk': self.item_1.pk,
            'user': 'http://testserver/api/users/{}/'.format(self.user.pk),
            'last_modified': '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.listpaper.last_modified),
            'created': '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.listpaper.created),
            'items': [
                OrderedDict([
                    ('pk', self.item_1.pk),
                    ('url', 'http://testserver/api/listpaperitems/{}/'.format(self.item_1.pk)),
                    ('text', 'Item 1'),
                    ('list', 'http://testserver/api/listpapers/{}/'.format(self.listpaper.pk)),
                    ('created', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_1.created)),
                    ('last_modified', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_1.last_modified)),
                ]),
                OrderedDict([
                    ('pk', self.item_2.pk),
                    ('url', 'http://testserver/api/listpaperitems/{}/'.format(self.item_2.pk)),
                    ('text', 'Item 2'),
                    ('list', 'http://testserver/api/listpapers/1/'.format(self.listpaper.pk)),
                    ('created', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_2.created)),
                    ('last_modified', '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_2.last_modified)),
                ])
            ],
        }

    ####################################################################################################################
    def test_get(self):  # GET
        """Should return a detail page for a single Listpaper object."""
        response = self.api_client.get(self.url)
        self.assert_status_ok(response, self.expected_data)

    ####################################################################################################################
    def test_create(self):  # POST
        """Should create ListpaperItem objects for the given Listpaper."""
        # Only 2 ListpaperItems should exist
        self.assertEqual(2, ListpaperItem.objects.count())

        # Do POST. Should succeed.
        data = {"text": "thank you"}
        with DateTimeMocker():
            response = self.api_client.post(self.url, data=data)

        # Should have created a new Listpaper.
        self.assertEqual(3, ListpaperItem.objects.count())

        # Should have the expected data
        pk = ListpaperItem.objects.last().pk
        expected_data = {'text': 'thank you',
                         'list': 'http://testserver/api/listpapers/{}/'.format(self.listpaper.pk),
                         'url': 'http://testserver/api/listpaperitems/{}/'.format(pk),
                         'created': self.mock_datetime_str,
                         'last_modified': self.mock_datetime_str,
                         'pk': pk}
        self.assert_created(response, expected_data)

    ####################################################################################################################
    def test_edit(self):  # PUT
        data = {"name": "thank you"}

        with DateTimeMocker():
            response = self.api_client.put(self.url, data=data)

        self.expected_data.update(data)
        self.expected_data["last_modified"] = self.mock_datetime_str
        self.assert_updated(response, self.expected_data)

    ####################################################################################################################
    def test_delete(self):  # DELETE
        response = self.api_client.delete(self.url)
        self.assert_deleted(response)
        response = self.api_client.get(self.url)
        self.assert_not_found(response, expected={"detail": "Not found."})


########################################################################################################################
class ListpaperItemDetailViewTest(BaseListpaperAPITest):

    ####################################################################################################################
    def setUp(self):
        super(ListpaperItemDetailViewTest, self).setUp()
        self.url = reverse("listpaperitem-detail", args=[self.item_1.pk])
        self.expected_data = {
            'text': self.item_1.text,
            'pk': self.item_1.pk,
            'list': 'http://testserver/api/listpapers/{}/'.format(self.listpaper.pk),
            'url': 'http://testserver/api/listpaperitems/{}/'.format(self.item_1.pk),
            'created': '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_1.created),
            'last_modified': '{:%Y-%m-%dT%H:%M:%S.%fZ}'.format(self.item_1.last_modified),
        }

    ####################################################################################################################
    def test_get(self):  # GET
        """Should return a detail page for a single ListpaperItem object."""
        response = self.api_client.get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(self.expected_data, response.data)

    ####################################################################################################################
    def test_create(self):  # POST
        """Should create ListpaperItem objects for the given Listpaper."""
        # Only the two test ListpaperItem objects exist.
        self.assertEqual(2, ListpaperItem.objects.count())

        # Do POST.
        data = {"text": "thank you"}
        with DateTimeMocker():
            response = self.api_client.post(self.url, data=data)

        # Should have a new ListpaperItem. Get it's pk.
        self.assertEqual(3, ListpaperItem.objects.count())

        # Response should have text 'thank you' and be related to self.listpaper
        pk = ListpaperItem.objects.last().pk
        expected_data = {'text': 'thank you',
                         'list': 'http://testserver/api/listpapers/{}/'.format(self.listpaper.pk),
                         'url': 'http://testserver/api/listpaperitems/{}/'.format(pk),
                         'created': self.mock_datetime_str,
                         'last_modified': self.mock_datetime_str,
                         'pk': pk}
        self.assert_created(response, expected=expected_data)

    ####################################################################################################################
    def test_edit(self):  # PUT
        """Should edit the existing ListpaperItem at this endpoint."""
        self.assertEqual(self.expected_data["text"], "Item 1")

        # Do PUT.
        data = {"text": "thank you"}
        with DateTimeMocker():
            response = self.api_client.put(self.url, data=data)

        # Should have updated 'text' to 'thank you'
        self.expected_data.update(data)

        self.expected_data["last_modified"] = self.mock_datetime_str
        self.assert_updated(response, expected=self.expected_data)

    ####################################################################################################################
    def test_delete(self):  # DELETE
        """Should delete the existing ListpaperItem at this endpoint."""
        # Should have 2 existing ListpaperItem objects
        self.assertEqual(2, ListpaperItem.objects.count())

        # Do DELETE.
        response = self.api_client.delete(self.url)
        self.assert_deleted(response, expected="Listpaper Item deleted.")

        # Should only have 1 ListpaperItem object now
        self.assertEqual(1, ListpaperItem.objects.count())

        # No ListpaperItem should be found at this url anymore.
        response = self.api_client.get(self.url)
        self.assert_not_found(response, expected={'detail': 'Not found.'})
