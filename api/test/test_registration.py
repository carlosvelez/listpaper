from django.core.urlresolvers import reverse
from rest_framework import status

from accounts.models import User
from api.test.utils import BaseListpaperAPITest


########################################################################################################################
class RegisterTest(BaseListpaperAPITest):

    ####################################################################################################################
    def test_create_user(self):
        url = reverse('account-list')
        data = {'email': 'dev.user@listpaper.io'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().name, 'Test User')
