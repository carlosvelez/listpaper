from datetimemocker.datetimemocker import DateTimeMocker
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APIClient

from api.models import Listpaper, ListpaperItem
from listpaper.test.utils import BaseListpaperTest


########################################################################################################################
class ResponseStatusCodeMixin(object):
    _success = 200
    _updated = 200
    _deleted = 204
    _created = 201
    _not_found = 404
    _not_allowed = 405

    ####################################################################################################################
    def assert_status_ok(self, response, expected=None):
        self.assertEqual(self._success, response.status_code)
        if expected:
            self.assertEqual(expected, response.data)

    ####################################################################################################################
    def assert_not_allowed(self, response, expected=None):
        self.assertEqual(self._not_allowed, response.status_code)
        if expected:
            self.assertEqual(expected, response.data)

    ####################################################################################################################
    def assert_created(self, response, expected=None):
        self.assertEqual(self._created, response.status_code)
        if expected:
            self.assertEqual(expected, response.data)

    ####################################################################################################################
    def assert_updated(self, response, expected=None):
        self.assertEqual(self._updated, response.status_code)
        if expected:
            self.assertEqual(expected, response.data)

    ####################################################################################################################
    def assert_deleted(self, response, expected=None):
        self.assertEqual(self._deleted, response.status_code)
        if expected:
            self.assertEqual(expected, response.data)

    ####################################################################################################################
    def assert_not_found(self, response, expected=None):
        self.assertEqual(self._not_found, response.status_code)
        if expected:
            self.assertEqual(expected, response.data)

    ####################################################################################################################
    def compare_each_item_in_response(self, expected, actual):
        for field, value in actual.items():
            msg = "Field '{field}':\n--{expected}\n--{actual}"
            msg = msg.format(field=field, expected=expected[field], actual=value)
            self.assertEqual(expected[field], actual, msg=msg)


########################################################################################################################
class BaseListpaperAPITest(BaseListpaperTest, APITestCase, ResponseStatusCodeMixin):

    ####################################################################################################################
    @classmethod
    def setUpTestData(cls):
        super(BaseListpaperAPITest, cls).setUpTestData()
        cls.mock_datetime_str = "{:%Y-%m-%dT%H:%M:%S.%fZ}".format(DateTimeMocker.default)
        cls.token = Token.objects.create(user=cls.user)
        cls.api_client = APIClient()
        cls.api_client.credentials(HTTP_AUTHORIZATION="Token {}".format(cls.token.key))
        cls.url = None

    ####################################################################################################################
    def setUp(self):
        super(BaseListpaperAPITest, self).setUp()
        self.listpaper = Listpaper.objects.create(name="Test List", user=self.user)
        self.item_1 = ListpaperItem.objects.create(text="Item 1", list=self.listpaper)
        self.item_2 = ListpaperItem.objects.create(text="Item 2", list=self.listpaper)
        self.expected_data = {}
