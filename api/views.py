from django.shortcuts import get_object_or_404
from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse as api_reverse
from rest_framework.views import APIView

from api import permissions as api_permissions
from api.models import Listpaper, ListpaperItem
from api.serializers import ListpaperItemSerializer, ListpaperSerializer


########################################################################################################################
@api_view(["GET"])
def api_root(request, format=None):
    return Response({
        "users": api_reverse("users", request=request, format=format),
        "listpapers": api_reverse("listpapers", request=request, format=format),
    })


########################################################################################################################
class ListpaperAPIView(APIView):
    """
    API endpoint that allows Listpaper objects to be viewed or edited.
    """
    serializer_class = ListpaperSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, api_permissions.IsOwnerOrReadOnly)

    ####################################################################################################################
    def get_queryset(self):
        return Listpaper.objects.filter(user=self.request.user)

    ####################################################################################################################
    def get(self, request, format=None):
        listpapers = self.get_queryset()
        serializer = ListpaperSerializer(listpapers, many=True, context={"request": request})
        return Response(serializer.data)

    ####################################################################################################################
    def post(self, request, format=None):
        serializer = ListpaperSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(user=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


########################################################################################################################
class ListpaperDetailAPIView(APIView):
    """
    API endpoint that allows viewing a Listpaper and it's items.
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, api_permissions.IsOwnerOrReadOnly)

    ####################################################################################################################
    def get_object(self, pk):
        return get_object_or_404(Listpaper, pk=pk)

    ####################################################################################################################
    def get(self, request, pk, format=None):
        listpaper = self.get_object(pk)
        serializer = ListpaperSerializer(listpaper, context={"request": request})
        return Response(serializer.data)

    ####################################################################################################################
    def put(self, request, pk, format=None):
        listpaper = self.get_object(pk)
        serializer = ListpaperSerializer(listpaper, data=request.data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    ####################################################################################################################
    def delete(self, request, pk, format=None):
        listpaper = self.get_object(pk)
        listpaper.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    ####################################################################################################################
    def post(self, request, pk, format=None):
        serializer = ListpaperItemSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(list=self.get_object(pk))
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


########################################################################################################################
class ListpaperItemDetailAPIView(APIView):
    """
    API endpoint for a ListpaperItem's detailed information.
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    ####################################################################################################################
    def get_object(self, pk):
        return get_object_or_404(ListpaperItem, pk=pk)

    ####################################################################################################################
    def get(self, request, pk, format=None):
        lp_item = self.get_object(pk=pk)
        serializer = ListpaperItemSerializer(lp_item, context={"request": request})
        return Response(serializer.data)

    ####################################################################################################################
    def put(self, request, pk, format=None):
        item = self.get_object(pk=pk)
        serializer = ListpaperItemSerializer(item, data=request.data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    ####################################################################################################################
    def post(self, request, pk, format=None):
        serializer = ListpaperItemSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(list=self.get_object(pk).list)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    ####################################################################################################################
    def delete(self, request, pk, format=None):
        item = self.get_object(pk=pk)
        item.delete()
        return Response("Listpaper Item deleted.", status=status.HTTP_204_NO_CONTENT)
