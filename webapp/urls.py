from django.conf.urls import url

import webapp.views as webapp_views


########################################################################################################################
urlpatterns = [
    url(r'^$', view=webapp_views.HomeView.as_view(), name="home"),
]
