import django.views.generic as django_views
import requests
from django.shortcuts import render
from rest_framework.reverse import reverse_lazy

from api.models import Listpaper
from base_views import DotworkAuthMixin


########################################################################################################################
class HomeView(DotworkAuthMixin, django_views.ListView):
    model = Listpaper
    context_object_name = "listpapers"
    template_name = "webapp/listpapers.html"
    url_name = "listpapers"

    ####################################################################################################################
    def get_url(self, request):
        return reverse_lazy(self.url_name, request=request)

    ####################################################################################################################
    def get(self, request, **kwargs):
        # todo: figure out how to write this as single-page style app that shows whatever
        # context of "list(s)" the user requests.
        url = self.get_url(request)
        headers = {"content-type": "application/json",
                   "Authorization": "Token {}".format(request.user.token)}
        lp_data = requests.get(url, headers=headers)
        context = {"listpapers": lp_data, "api_url": url}
        return render(request, template_name=self.template_name, context=context)
