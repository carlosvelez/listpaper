from django.contrib.auth.models import Group
from rest_framework import serializers

import accounts.models as account_models


########################################################################################################################
class UserSerializer(serializers.HyperlinkedModelSerializer):

    ####################################################################################################################
    class Meta:
        model = account_models.User
        fields = ('url', 'email', 'groups', 'pk')


########################################################################################################################
class GroupSerializer(serializers.HyperlinkedModelSerializer):

    ####################################################################################################################
    class Meta:
        model = Group
        fields = ('url', 'name', 'pk')
