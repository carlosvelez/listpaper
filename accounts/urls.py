from django.conf.urls import url
from django.contrib.auth.views import login, logout

from accounts import views as account_views

########################################################################################################################
user_list = account_views.UserList.as_view({
    "get": "list",
})

########################################################################################################################
urlpatterns = [
    url(r"^api/users/$", user_list, name="users"),
    url(r"^api/users/(?P<pk>\d+)/$", account_views.UserDetail.as_view(), name="user-detail"),
    url(r"^login/$", login, name="login"),
    url(r"^logout/$", logout, name="logout", kwargs={"next_page": "/login"}),
    url(r"^register/$", account_views.RegistrationView.as_view(), name="register"),
]
