# import datetime
# from dotwork.tests.utils import BaseDotworkTest, TestTimestampMixin
# from mylibrary.datetime_utils import UTC, CST, DatetimeHandlingException
#
#
# ########################################################################################################################
# class BaseUserTest(BaseDotworkTest):
#     pass
#
#
# ########################################################################################################################
# class UserLocalTimeTest(TestTimestampMixin, BaseUserTest):
#
#     ####################################################################################################################
#     def assert_utc_to_local_conversion(self, utc, local):
#         converted = self.user.local_time(utc)
#         self.assertEqual(local, converted)
#
#     ####################################################################################################################
#     def test_12_am(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 7, 8, hour=5)),
#                                             CST.localize(datetime.datetime(2016, 7, 8, hour=0)))
#
#     ####################################################################################################################
#     def test_12_am__dst(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 1, 8, hour=6)),
#                                             CST.localize(datetime.datetime(2016, 1, 8, hour=0)))
#
#     ####################################################################################################################
#     def test_8_am(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 7, 8, hour=13)),
#                                             CST.localize(datetime.datetime(2016, 7, 8, hour=8)))
#
#     ####################################################################################################################
#     def test_8_am__dst(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 1, 8, hour=14)),
#                                             CST.localize(datetime.datetime(2016, 1, 8, hour=8)))
#
#     ####################################################################################################################
#     def test_12_pm(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 7, 8, hour=17)),
#                                             CST.localize(datetime.datetime(2016, 7, 8, hour=12)))
#
#     ####################################################################################################################
#     def test_12_pm__dst(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 1, 8, hour=18)),
#                                             CST.localize(datetime.datetime(2016, 1, 8, hour=12)))
#
#     ####################################################################################################################
#     def test_1_pm(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 7, 8, hour=18)),
#                                             CST.localize(datetime.datetime(2016, 7, 8, hour=13)))
#
#     ####################################################################################################################
#     def test_1_pm__dst(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 1, 8, hour=19)),
#                                             CST.localize(datetime.datetime(2016, 1, 8, hour=13)))
#
#     ####################################################################################################################
#     def test_9_pm(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 7, 9, hour=2)),
#                                             CST.localize(datetime.datetime(2016, 7, 8, hour=21)))
#
#     ####################################################################################################################
#     def test_9_pm__dst(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 1, 8, hour=14)),
#                                             CST.localize(datetime.datetime(2016, 1, 8, hour=8)))
#
#     ####################################################################################################################
#     def test_11_59_pm(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 7, 9, hour=4, minute=59)),
#                                             CST.localize(datetime.datetime(2016, 7, 8, hour=23, minute=59)))
#
#     ####################################################################################################################
#     def test_11_59_pm__dst(self):
#         self.assert_utc_to_local_conversion(UTC.localize(datetime.datetime(2016, 1, 9, hour=5, minute=59)),
#                                             CST.localize(datetime.datetime(2016, 1, 8, hour=23, minute=59)))
#
#     ####################################################################################################################
#     def test_no_tzinfo(self):
#         with self.assertRaisesMessage(DatetimeHandlingException, "Must provide UTC tzinfo."):
#             self.assert_utc_to_local_conversion(datetime.datetime(2016, 1, 9, hour=5),
#                                                 CST.localize(datetime.datetime(2016, 1, 8, hour=0)))
#
#     ####################################################################################################################
#     def test_tzinfo_not_utc(self):
#         with self.assertRaisesMessage(DatetimeHandlingException, "Must provide UTC tzinfo."):
#             self.assert_utc_to_local_conversion(CST.localize(datetime.datetime(2016, 1, 8, hour=5)),
#                                                 CST.localize(datetime.datetime(2016, 1, 8, hour=0)))
