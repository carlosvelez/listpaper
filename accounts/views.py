from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.db import transaction
from django.shortcuts import render, redirect
from django.views import generic
from rest_framework import viewsets, generics, permissions

from accounts import forms as account_forms
from accounts.models import User
from accounts.serializers import UserSerializer, GroupSerializer
from api import permissions as api_permissions


########################################################################################################################
class UserList(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, api_permissions.IsOwnerOrReadOnly)


########################################################################################################################
class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, api_permissions.IsOwnerOrReadOnly)


########################################################################################################################
class GroupList(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, api_permissions.IsOwnerOrReadOnly)


########################################################################################################################
class RegistrationView(generic.View):
    form_class = account_forms.RegistrationForm
    template_name = "registration/registration.html"

    ####################################################################################################################
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {"form": form})

    ####################################################################################################################
    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():

            with transaction.atomic():
                user = form.save(commit=False)
                email = form.cleaned_data["email"]
                password = form.cleaned_data["password"]
                password2 = form.cleaned_data["password2"]
                if not password == password2:
                    raise ValidationError("Passwords do not match.")

                user.username = email
                user.is_active = True
                user.set_password(password)
                user.save()

            user = authenticate(username=user.username, password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    from settings import LOGIN_REDIRECT_URL
                    return redirect(LOGIN_REDIRECT_URL)

        return render(request, self.template_name, {"form": form})
