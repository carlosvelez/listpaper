from django import forms
from django.core.exceptions import ValidationError

from accounts.models import User


########################################################################################################################
class RegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput, label="Confirm password")

    ####################################################################################################################
    class Meta:
        model = User
        fields = ['email', 'password', 'password2']

    ####################################################################################################################
    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        password = cleaned_data["password"]
        password2 = cleaned_data["password2"]
        if not password == password2:
            raise ValidationError("Passwords do not match.")
        return cleaned_data
