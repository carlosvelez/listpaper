from django.contrib import admin
import accounts.models as account_models


########################################################################################################################
@admin.register(account_models.User)
class UserAdmin(admin.ModelAdmin):
    pass
