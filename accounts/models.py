import pytz
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from rest_framework.authtoken.models import Token

from base_models import TimestampedModelMixin
########################################################################################################################
from utils import DatetimeHandlingException


class UserManager(BaseUserManager):

    ####################################################################################################################
    def create_user(self, email, password, **kwargs):
        user = self.model(
            email=self.normalize_email(email),
            is_active=True,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    ####################################################################################################################
    def create_superuser(self, email, password, **kwargs):
        user = self.model(
            email=email,
            is_staff=True,
            is_superuser=True,
            is_active=True,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user


########################################################################################################################
class User(AbstractBaseUser, PermissionsMixin, TimestampedModelMixin):
    USERNAME_FIELD = 'email'
    TIMEZONE_CHOICES = ((tz, tz) for tz in pytz.common_timezones)
    objects = UserManager()

    email = models.EmailField(unique=True)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    timezone_slug = models.CharField(max_length=40, choices=TIMEZONE_CHOICES, default="US/Central")

    @property
    ####################################################################################################################
    def token(self):
        return Token.objects.get(user=self)

    @property
    ####################################################################################################################
    def timezone(self):
        return pytz.timezone(self.timezone_slug)

    ####################################################################################################################
    def get_full_name(self):
        return self.email

    ####################################################################################################################
    def get_short_name(self):
        return self.email

    ####################################################################################################################
    def local_time(self, utc_datetime_obj):
        try:
            assert utc_datetime_obj.tzinfo == pytz.utc
        except AssertionError:
            raise DatetimeHandlingException("Must provide UTC tzinfo.")
        return utc_datetime_obj.astimezone(self.timezone)

    @property
    ####################################################################################################################
    def local_now(self):
        return self.local_time(timezone.now())
